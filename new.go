package email

// NewEmail takes a string then parses and validates the string against an email address specification
// an Email is returned regardless of whether the email address is valid but validity can be checked
// using the IsValid property on the object
func NewEmail(s string) Email {
	o := Email{Raw: s, CharMap: make(map[byte][]uint8)}
	// check for display name and parse
	o.parseDisplayName()
	// check overall length
	o.checkLength()
	if o.IsValid {
		// split the local-part from the FQDN
		o.split()
		if o.IsValid {
			o.validateLocal()
			o.validateFQDN()
		}
	}
	return o
}
