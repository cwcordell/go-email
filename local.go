import "fmt"

// validateLocal determines if the local-part of the email address conforms to RFC 5322
func (o *Email) validateLocal() {
	// email address
	s := o.Address

	// last character evaluated
	var last rune

	// lastChar is the last character of the local-part
	lastChar := rune(s[len(s)-1])

	// pre-checking the last character will save compute cost if true
	if !validNormChar(lastChar) && lastChar != dqt && lastChar != cp {
		o.IsValid = false
		o.addIMsg(fmt.Sprintf("A %s is not allowed to be the last character of the local-part", string(lastChar)))
	}

	var r rune
	for i := 0; i < len(s); i++ {
		if r == dot {
			if last == dot {
				o.addIMsg(fmt.Sprintf(`Invalid character placement at index %d\nConsecutive dots are not allowed in unquoted sections`, i))
				last = r
				o.IsValid = false
				return
			} else if last == 0 {
				o.addIMsg(fmt.Sprintf(`Invalid character placement at index %d\nA dot is not allowed at the beginning`, i))
				last = r
				o.IsValid = false
				return
			}
		} else if r == op {
			if o.HasLocalComment {
				o.addIMsg(fmt.Sprintf(`Invalid character placement at index %d\nOnly one comment is permitted`, i))
				last = r
				o.IsValid = false
				return
			}
			o.HasLocalComment = true
			j, c, m := packComment(i, s)
			o.LocalComment = c
			i = j
			last = rune(s[i])
			if len(m) > 0 {
				o.InvalidMessages = append(o.InvalidMessages, m...)
			}
		} else {
			// a double quote is the first char of the section
			quoted := false
			// builder for the section
			section := []rune{}

			if r == dqt {
				quoted = true
				section = append(section, r)
				last = r
				i++
				r = rune(s[i])
			}

			for {
				if r == dot && !quoted {
					o.LocalSections = append(o.LocalSections, string(section))
					break
				} else if r == bs {
					if !quoted || i+1 >= len(s) {
						o.IsValid = false
						o.addIMsg(fmt.Sprintf(`Invalid character placement at index %d\nA backslash must be quoted and escaped`, i))
						return
					}
					next := rune(s[i+1])
					if next == dqt || next == bs {
						section = append(section, r)
						last = r
						i++
					} else {
						o.IsValid = false
						o.addIMsg(fmt.Sprintf(`Invalid character placement at index %d\nA backslash must be quoted and escaped`, i))
						return
					}
				} else {
					if !validNormChar(r) || !(quoted && isAllowed(r, LAQSC)) {
						o.IsValid = false
						o.addIMsg(fmt.Sprintf("A %s is not an allowed character of the local-part", string(lastChar)))
					}
				}
				section = append(section, r)
				last = r
				i++
				// check if reached end of string
				if i >= len(s) {
					o.LocalSections = append(o.LocalSections, string(section))
					if quoted {
						o.IsValid = false
						o.addIMsg("Missing ending double quote")
						return
					}
					break
				}
				r = rune(s[i])
			}
		}
	}

	if len(o.InvalidMessages) > 0 {
		o.IsValid = false
		return
	}
	o.IsValid = true
}
