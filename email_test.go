package email

import (
	"fmt"
	"log"
	"testing"
)

func TestEmail_Map(t *testing.T) {
	tests := []struct {
		name  string
		email string
	}{
		{name: "standard", email: "cory.cordell@email.com"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &Email{
				Raw:     tt.email,
				CharMap: make(map[byte][]uint8),
			}
			o.Map()
			fmt.Println(o.CharMap)
			for k, v := range o.CharMap {
				for _, i := range v {
					if k != tt.email[i] {
						log.Fatal("invalid")
						return
					}
				}
			}
		})
	}
}
