// This package provides validation for email addresses based on RFC 5322
// Links:
// https://tools.ietf.org/html/rfc5322
// https://en.wikipedia.org/wiki/Email_address

package email

import (
	"fmt"
	"strings"
)

// IEmail is an interface for an Email Address
type iEmail interface {
	Valid()
}

const (
	// EmailMinLength is the minimum overall character count an email can be
	EmailMinLength uint8 = 3
	// EmailMaxLength is the maximum overall character count an email can be
	// computed by LocalCharLimit + @ + FQDNCharLimit
	EmailMaxLength uint = uint(LocalCharLimit) + 1 + uint(FQDNCharLimit)
	// LocalCharLimit is the number of characters allowed in the local-part
	LocalCharLimit uint8 = 64
	// FQDNCharLimit is the number of characters allowed in the FQDN
	FQDNCharLimit uint8 = 255
	// LabelCharLimit is the number of characters allowed in a domain label (dot separated segment of the domain)
	LabelCharLimit uint8 = 63
)

var (
	// LASC (Local-part Allowed Special Characters) is an array of the special characters allowed to be in the local-part
	LASC = []rune("!#$%&'*+-/=?^_`{|}~")

	// LAQSC (Local-part Allowed Quoted Special Characters) is an array of the special characters allowed to be in the local-part when quoted
	LAQSC = []rune("(),:;<>@[]")

	// LAQESC (Local-part Allowed Quoted Escaped Special Characters) is an array of the special characters allowed to be in the local-part when quoted and escaped
	LAQESC = []rune(".\\")

	// rune representation of dot
	dot = '.'
	// rune representation of a space
	space = ' '
	// rune representation of a double quote
	dqt = '"'
	// rune representation of the at symbol
	at = '@'
	// rune representation of the open parenthesis
	op = '('
	// rune representation of the closing parenthesis
	cp = '('
	// rune representation of the backslash
	bs = '\\'
)

// Email provides storage and methods for handling email addresses
type Email struct {
	// the original email address
	Raw string
	// local-part or often the username of the recipient, e.g. cwcordell in cwcordell@email.com
	Local string
	// DisplayName (not required) can come before the address specification surrounded by angled brackets, e.g. Cory Cordell <cory.cordell@email.com>
	DisplayName string
	// Address is the part of the email address without the display name designation syntax, e.g. cory.cordell@email.com in Cory Cordell <cory.cordell@email.com>
	Address string
	// local-part can contain comments, designated by surrounding parenthasis, either at the beginning or at the end
	// e.g. cory.cordell(comment)@example.com and (comment)cory.cordell@example.com
	LocalComment string
	// HasLocalComment indicates whether the local-part contains a comment
	HasLocalComment bool
	// LocalIsQuoted indicates if the local-part is surrounded by double quotes, which has additional allowances for the use of special characters
	// e.g. ".cory..cordell."@email.com
	LocalIsQuoted bool
	// FQDN (fully quallified domain name) is the part of the email address after the @ symbol, e.g. email.com in cory.cordell@email.com
	FQDN string
	// FQDN's can contain comments, designated by surrounding parenthasis, either after the @ symbol or at the end
	// e.g. cory.cordell@(comment)example.com and cory.cordell@example.com(comment)
	FQDNComment string
	// HasFQDNComment indicates whether the FQDN contains a comment
	HasFQDNComment bool
	// dot separated sections for local-part
	LocalSections []string
	// dot separated sectiond for the FQDN
	DomainSections []string
	// indicates if the email address is valid according to RFC 5322
	IsValid bool
	// InValidMessages holds any notations as to why an email address is invalid
	InvalidMessages []string
	// Length is the number of characters in the email address
	Length uint8
	// CharMap is a map with the keys as the characters (string) in the email address and the values as are
	// slices containing the index in which that key occurred in the email address
	CharMap map[byte][]uint8
}

func (o *Email) addIMsg(s ...string) {
	o.InvalidMessages = append(o.InvalidMessages, s...)
}

// split will add an error if the email string does not contain an @ symbol
// otherwise the Local propertiy is populated with the substring up to the last @ symbol
// and the FQDN property is populated with the remainder after the last @ symbol
func (o *Email) split() {
	lastAt := strings.LastIndex(o.Raw, "@")
	lastQuote := strings.LastIndex(o.Raw, "\"")
	if lastQuote > lastAt {
		o.addIMsg("Improper email format. \nThe @ symbol appears to be either part of a quoted local or a \" is part of the domain, which is not an allowed character")
		return
	}
	a := strings.Split(o.Raw, "@")
	l := len(a)
	// handle local-part@domain scenario
	if l < 2 {
		o.addIMsg("The email address does not contain the required '@' symbol separating the local-part from the FQDN")
		return
	}
	// handle more than one @ symbol condition
	o.Local = strings.Join(a[0:l-1], "@")
	o.FQDN = a[l-1]
}

func (o *Email) checkLength() {
	o.Length = uint8(len(o.Raw))
	if o.Length < EmailMinLength {
		o.addIMsg(fmt.Sprintf("Overall email address length does not meet the minimum requirement of %d characters", EmailMinLength))
	} else if uint(o.Length) > EmailMaxLength {
		o.addIMsg(fmt.Sprintf("Overall email address length exceedes the maximum length allowed of %d characters", EmailMaxLength))
	}
}

func (o *Email) parseDisplayName() {
	last := strings.LastIndex(o.Raw, ">")
	first := strings.Index(o.Raw, "<")
	if last > 0 {
		if first < 0 || last != len(o.Raw)-1 {
			o.addIMsg(fmt.Sprintf("Invalid character placement at index %d\nA right-angle bracket (>) is not allowed unless at the end of the addreess as part of formatting when using a display name", last))
		}
		o.DisplayName = strings.TrimSpace(o.Raw[0:first])
		o.Address = o.Raw[first+1 : last]
		return
	}
	o.Address = o.Raw
}
