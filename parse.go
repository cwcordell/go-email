package email

func packComment(i int, s string) (int, string, []string) {
	var r rune
	comment := []rune{}
	msg := []string{}

	for {
		// check if reached end of string
		if i >= len(s) {
			msg = append(msg, "Comment was not closed")
			return i, string(comment), msg
		}

		// process next character
		r = rune(s[i])
		// return if closing parenthesis found
		if r == cp {
			return i, string(comment), msg
		}
		comment = append(comment, r)
		i++
	}
}
