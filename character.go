package email

func validNormChar(r rune) bool {
	if isLetter(r) || isDigit(r) || isAllowed(r, LASC) {
		return true
	}
	return false
}

func isAllowed(r rune, a []rune) bool {
	for i := 0; i < len(a); i++ {
		if r == a[i] {
			return true
		}
	}
	return false
}

func isLetter(r rune) bool {
	if isLCLetter(r) || isUCLetter(r) {
		return true
	}
	return false
}

func isLCLetter(r rune) bool {
	if r < 'a' || r > 'z' {
		return false
	}
	return true
}

func isUCLetter(r rune) bool {
	if r < 'A' || r > 'Z' {
		return false
	}
	return true
}

func isDigit(r rune) bool {
	if r < '0' || r > '9' {
		return false
	}
	return true
}
